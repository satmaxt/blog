<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Label;
use Auth;

class LabelController extends Controller
{
    private $_role;

    public function __construct()
    {
      if(Auth::user()->role_id == 2) {
        $this->_role = "member";
      } else {
        $this->_role = "admin";
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['labels'] = Label::paginate(8);
        return view('admin.label.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['labels'] = Label::paginate(8);
        return view('partials._labelLists', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if(Request::ajax()) {
            $validator = \Validator::make(Request::all(), [
                'label' => 'required|min:3|max:255|unique:labels',
            ]);

            if($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->toArray(),
                ]);
            }

            Label::create(Request::all());

            return response()->json([
                'success' => true,
                'message' => 'Selamat, label baru telah berhasil di tambahkan.',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['label'] = Label::find($id);
        return view('partials._modalLabel', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if(Request::ajax()) {
            $data = Request::all();
            $validator = \Validator::make($data, [
                'label' => 'required|min:3|max:255|unique:labels',
            ]);

            if($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->toArray(),
                ]);
            }

            $init = Label::find($id);
            $init->label = $data['label'];
            $init->save();

            return response()->json([
                'success' => true,
                'message' => 'Selamat, label baru telah berhasil di tambahkan.',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $init = Label::find($id);
        if($init->Portfolios()->count() > 0) {
          return response()->json([
              'success' => false,
              'message' => 'Gagal, label ini masih mempunyai portfolio yang terkait, hapus terlebih dahulu portfolio tersebut',
          ]);
        }

        Label::destroy($id);
        return response()->json([
            'success' => true,
            'message' => 'Selamat, label telah berhasil di hapus.',
        ]);
    }
}
