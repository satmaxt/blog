<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Label;
use App\Models\Portfolio;
use Auth;

class PortfolioController extends Controller
{
    private $_role;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      if(Auth::user()->role_id == 2) {
        $this->_role = "member";
      } else {
        $this->_role = "admin";
      }
    }

    public function index()
    {
      if(Auth::user()->role_id == 2) {
        $data['portfolios'] = Portfolio::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(10);
      } else {
        $data['portfolios'] = Portfolio::orderBy('id', 'desc')->paginate(10);
      }
      return view($this->_role.'.portfolios.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['labels'] = Label::all();
        return view($this->_role.'.portfolios.create', $data);
    }

    protected function uploadThumb($file) {
      $data = $file;
      $name = time().'_'.str_replace(' ', '_', $data->getClientOriginalName());
      $path = public_path().'/image';
      $data->move($path, $name);
      return $name;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
          'judul' => 'required',
          'desc' => 'required',
          'label' => 'required',
          'thumbnail' => 'required|mimes:jpeg,png,jpg',
        ]);

        if($validator->fails()) {
          return redirect()->route($this->_role.'.portfolio.create')->withErrors($validator)->withInput();
        }

        $label = Label::find($request->label);
        if(empty($label)) {
          \Session::flash('flash_message', [
            'level' => 'danger',
            'subject' => 'Peringatan!',
            'message' => 'Kategori yang anda masukan tidak benar',
          ]);
          return redirect()->route($this->_role.'.portfolio.create')->withErrors($validator)->withInput();
        }

        $init = Portfolio::create([
          'user_id' => Auth::user()->id,
          'judul' => $request->judul,
          'kelompok' => $request->namakelompok,
          'anggota' => $request->anggota,
          'author' => $request->namaindividu,
          'thumbnail' => $this->uploadThumb($request->thumbnail),
          'desc' => $request->desc,
          'embed' => $request->embed,
          'durasi' => $request->durasi,
          'slug' => str_slug($request->judul, '-'),
        ]);

        $init->Labels()->attach($label->id);

        \Session::flash('flash_message', [
          'message' => 'Portfolio terbaru anda sudah berhasil di publish',
        ]);
        return redirect()->route($this->_role.'.portfolio.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['portfolio'] = Portfolio::find($id);

        // check user id of portfolio
        if($data['portfolio']->user_id != Auth::user()->id) {
          \Session::flash('flash_message', [
            'message' => 'Error. Kamu tidak punya akses untuk mengedit portfolio tadi.',
          ]);
          return redirect()->route($this->_role.'.portfolio.index');
        }
        $data['labels'] = Label::where('id', '!=', $data['portfolio']->Labels()->first()->id)->get();
        return view($this->_role.'.portfolios.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = \Validator::make($request->all(), [
        'judul' => 'required',
        'desc' => 'required',
        'label' => 'required',
        'thumbnail' => 'mimes:jpg,png',
      ]);

      if($validator->fails()) {
        return redirect()->route($this->_role.'.portfolio.edit',['id'=>$id])->withErrors($validator)->withInput();
      }

      $label = Label::find($request->label);
      $init = Portfolio::find($id);

      if(empty($label)) {
        \Session::flash('flash_message', [
          'level' => 'danger',
          'subject' => 'Peringatan!',
          'message' => 'Kategori yang anda masukan tidak benar',
        ]);
        return redirect()->route($this->_role.'.portfolio.edit',['id'=>$id])->withErrors($validator)->withInput();
      }

      if(empty($request->thumbnail)) {
        $init->judul = $request->judul;
        $init->kelompok = $request->namakelompok;
        $init->anggota = $request->anggota;
        $init->author = $request->namaindividu;
        $init->desc = $request->desc;
        $init->embed = $request->embed;
        $init->durasi = $request->durasi;
        $init->save();
      } else {
        $init->judul = $request->judul;
        $init->kelompok = $request->namakelompok;
        $init->anggota = $request->anggota;
        $init->author = $request->namaindividu;
        $init->desc = $request->desc;
        $init->thumbnail = $this->uploadThumb($request->thumbnail);
        $init->embed = $request->embed;
        $init->durasi = $request->durasi;
        $init->save();
      }

      $init->Labels()->updateExistingPivot($init->Labels()->first()->id, [
        'label_id' => $label->id,
      ]);

      \Session::flash('flash_message', [
        'message' => 'Portfolio anda sudah berhasil di update',
      ]);
      return redirect()->route($this->_role.'.portfolio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $init = Portfolio::find($id);
        $init->Labels()->detach($init->Labels()->first()->id);

        if($init->user_id != Auth::user()->id) {
          \Session::flash('flash_message', [
            'message' => 'Error. Kamu tidak punya akses untuk menghapus portfolio tadi.',
          ]);
          return redirect()->route($this->_role.'.portfolio.index');
        }

        Portfolio::destroy($id);

        \Session::flash('flash_message', [
          'message' => 'Portfolio anda sudah berhasil di hapus',
        ]);
        return redirect()->route($this->_role.'.portfolio.index');
    }
}
