<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Said;

class SaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['saids'] = Said::paginate(8);
        return view('admin.saids.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['saids'] = Said::paginate(8);
        return view('partials._resultFaker', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Request::ajax())
        {
            $data = Request::all();
            $validator = \Validator::make($data, [
                'name' => 'required|min:5|max:100',
                'passion' => 'required|min:5|max:100',
                'content' => 'required|min:50',
            ]);

            if($validator->fails()){
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->toArray(),
                ]);
            }

            Said::create(Request::all());

            return response()->json([
                'success' => true,
                'message' => 'Selamat, data said telah ditambahkan.',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = Said::find($id);
        return view('partials._modalFaker', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Request::all();
        $validator = \Validator::make($data, [
            'name' => 'required|min:5|max:100',
            'passion' => 'required|min:5|max:100',
            'content' => 'required|min:50',
        ]);

        if($validator->fails()){
            return redirect()->route('admin.saids.index')->withErrors($validator);
        }

        Said::where('id', $id)->update([
            'name' => $data['name'],
            'passion' => $data['passion'],
            'content' => $data['content']
        ]);

        \Session::flash('flash_message', [
            'message' => 'Selamat, data barusaja terupdate.'
        ]);

        return redirect()->route('admin.saids.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Said::destroy($id);

        \Session::flash('flash_message', [
            'message' => 'Data telah berhasil dihapus.'
        ]);

        return redirect()->route('admin.saids.index');
    }
}
