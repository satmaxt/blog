<?php

namespace App\Http\Controllers\Admin;

// use Illuminate\Http\Request;
use Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Service;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['services'] = Service::orderBy('id', 'desc')->paginate(8);
        return view('admin.service.index', $data);
    }
    public function showAction()
    {
        $data['services'] = Service::orderBy('id', 'desc')->paginate(8);
        return view('partials._serviceLists', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if(Request::ajax()) {
            $input = Request::all();

            $validator = \Validator::make($input, [
                'title' => 'required|min:5|max:255',
                'desc' => 'required|min:5|max:255',
                'icon' => 'required|min:2|max:20',
            ]);

            if($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->toArray(),
                ]);
            }

            Service::create(Request::all());

            $Response = [
                'pesan' => 'Selamat, service baru telah ditambahkan',
            ];

            return response()->json(['success'=>true, 'message'=>$Response]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['service'] = Service::find($id);
        return view('admin.service.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Request::all();

        $validator = \Validator::make($input, [
            'title' => 'required|min:5|max:255',
            'desc' => 'required|min:5|max:255',
            'icon' => 'required|min:2|max:20',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.service.edit', ['id'=>$id])->withErrors($validator);
        }

        Service::where('id', $id)->update([
            'title' => $input['title'],
            'desc' => $input['desc'],
            'icon' => $input['icon'],
        ]);

        \Session::flash('flash_message', [
            'message' => 'Selamat, service telah diubah',
        ]);

        return redirect()->route('admin.service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::destroy($id);

        \Session::flash('flash_message', [
            'message'=>'Sukses menghapus data service',
        ]);

        return redirect()->route('admin.service.index');
    }
}
