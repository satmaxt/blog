<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['teachers'] = Teacher::all();
        return view('admin.teacher.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nama' => 'required|max:100',
            'mapel' => 'required|max:100',
            'foto' => 'required|mimes:jpeg,bmp,png',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.teacher.index')->withErrors($validator);
        }

        $image = $request->foto;
        $path = public_path().'/image';
        $name = time().'_'.str_replace(' ', '_', $image->getClientOriginalName());
        $image->move($path, $name);

        Teacher::create([
            'nama' => $request->nama,
            'mapel' => $request->mapel,
            'foto' => $name,
        ]);

        \Session::flash('flash_message', [
            'subject' => 'Sukses!',
            'message' => 'Sukses menambahkan data guru',
        ]);
        return redirect()->route('admin.teacher.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['teacher'] = Teacher::find($id);
        return view('partials._modalTeacher',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'nama' => 'required|max:100',
            'mapel' => 'required|max:100',
            'foto' => 'mimes:jpeg,bmp,png',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.teacher.index')->withErrors($validator);
        }

        if(!empty($request->foto)) {
            $image = $request->foto;
            $path = public_path().'/image';
            $name = time().'_'.str_replace(' ', '_', $image->getClientOriginalName());
            $image->move($path, $name);

            Teacher::where('id', $id)->update([
                'nama' => $request->nama,
                'mapel' => $request->mapel,
                'foto' => $name,
            ]);
        } else {
            Teacher::where('id', $id)->update([
                'nama' => $request->nama,
                'mapel' => $request->mapel,
            ]);
        }
        
        \Session::flash('flash_message', [
            'subject' => 'Sukses!',
            'message' => 'Sukses menambahkan data guru',
        ]);
        return redirect()->route('admin.teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::destroy($id);
        \Session::flash('flash_message', [
            'subject' => 'Sukses!',
            'message' => 'Sukses menghapus data guru',
        ]);
        return redirect()->route('admin.teacher.index');
    }
}
