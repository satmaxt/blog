<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('id', '!=', \Auth::user()->id)->paginate(4);
        return view('admin.userList', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if(Request::ajax()) {
            $data = Request::all();
            // var_dump($data);
            // die;
            $validator = \Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|unique:users|min:6|max:15',
                'password' => 'required|min:8|max:16',
                'role' => 'required|in:1,2',
            ]);

            if($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->toArray(),
                ]);
            }

            if(\Auth::user()->role_id != 1) {
                $Response = [
                    'level' => 'warning',
                    'subject' => 'Error!',
                    'pesan' => 'Hanya admin yang bisa menambahkan user',
                ];

                return response()->json(['success'=>true, 'message'=>$Response]);
            }

            User::create([
                'role_id' => $data['role'],
                'name' => $data['name'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
            ]);

            $Response = [
                'level' => 'success',
                'subject' => 'Sukses!',
                'pesan' => 'Selamat, user baru telah ditambahkan',
            ];

            return response()->json(['success'=>true, 'message'=>$Response]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        return view('partials._modalUsers', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = Request::all();
        $valid = '';
        // var_dump($data);
        // die;
        if($user->username == $data['username']) {
            $valid = 'required|max:15';
        } else {
            $valid = 'required|unique:users|max:15';
        }
        $validator = \Validator::make($data, [
            'name' => 'required|max:255',
            'username' => $valid,
            'password' => 'max:8',
            'role' => 'required|in:1,2',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.user.index')->withErrors($validator);
        }

        $user->name = $data['name'];
        $user->username = $data['username'];
        if(!empty(trim($data['password']))) {
            $user->password = bcrypt($data['password']);
        }
        $user->role_id = $data['role'];
        $user->save();

        \Session::flash('flash_message', [
            'level' => 'success',
            'subject' => 'Sukses!',
            'pesan' => 'Selamat, data telah berhasil disimpan',
        ]);

        return redirect()->route('admin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
