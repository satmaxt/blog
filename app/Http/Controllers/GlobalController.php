<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Service;
use App\Http\Requests;
use App\Models\Label;
use App\Models\Said;
use App\Models\Teacher;

class GlobalController extends Controller
{
    public function homeIndex()
    {
      $data['about'] = Page::first();
      $data['services'] = Service::orderBy('id', 'desc')->take(6)->get();
      $data['labels'] = Label::take(5)->get();
      $data['saids'] = Said::orderBy('id', 'desc')->take(10)->get();
      return view('guest.index', $data);
    }

    public function about()
    {
      $data['about'] = Page::first();
      $data['teachers'] = Teacher::all();
      return view('guest.about', $data);
    }
}
