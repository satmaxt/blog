<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Social;
use App\User;
use App\Models\Page;
use App\Models\Option;
use App\Http\Requests;

class OptionsController extends Controller
{
    public function accountIndex()
    {
    	$data['user'] = User::find(\Auth::user()->id);
    	return view('admin.options.account', $data);
    }

    public function userInfo(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'username' => 'required|min:6|max:15',
            'password' => 'required|min:8|max:16',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return redirect()->route('admin.options.user')->withErrors($validator);
        }

        if(\Hash::check($request->password, \Auth::user()->password)) {
            User::where('id', \Auth::user()->id)->update([
                'name' => $request->name,
                'username' => $request->username,
            ]);

            \Session::flash('flash_message', [
                'level' => 'success',
                'subject' => 'Sukses!',
                'message' => 'Data telah berhasil di ganti.'
            ]);

            return redirect()->route('admin.options.user');
        } else {
            \Session::flash('flash_message', [
                'level' => 'warning',
                'subject' => 'Error!',
                'message' => 'Password yang anda masukan salah.'
            ]);

            return redirect()->route('admin.options.user');
        }
    }

    public function passSave(Request $request)
    {
    	$validator = \Validator::make($request->all(), [
    		'oldPass' => 'required|min:8|max:16',
    		'newPass' => 'required|min:8|max:16|confirmed',
    	]);

    	if($validator->fails()) {
    		return redirect()->route('admin.options.user')->withErrors($validator);
    	}

    	if(\Hash::check($request->oldPass, \Auth::user()->password)) {
    		User::where('id', \Auth::user()->id)->update([
            'password' => bcrypt($request->newPass),
            ]);

            \Session::flash('flash_message', [
                'level' => 'success',
                'subject' => 'Sukses!',
                'message' => 'Password lama telah berhasil diganti.'
            ]);

            return redirect()->route('admin.options.user');
    	} else {
            \Session::flash('flash_message', [
                'level' => 'warning',
                'subject' => 'Error!',
                'message' => 'Password lama yang anda masukan salah.'
            ]);

            return redirect()->route('admin.options.user');
        }
    }

    public function socialIndex()
    {
        $data['social'] = Social::first();
        return view('admin.options.socials', $data);
    }

    public function socialSave(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'facebook' => 'required|max:100',
            'twitter' => 'required|max:100',
            'gplus' => 'required|max:100',
            'instagram' => 'required|max:100',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.options.socials')->withErrors($validator);
        }
        $init = Social::first();
        Social::where('instagram', $init->instagram)->update([
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'gplus' => $request->gplus,
            'instagram' => $request->instagram,
        ]);

        \Session::flash('flash_message', [
            'level' => 'success',
            'subject' => 'Sukses!',
            'message' => 'Data sosial telah berhasil diganti.'
        ]);

        return redirect()->route('admin.options.socials');
    }

    public function siteIndex()
    {
        $data['site'] = Option::first();
        return view('admin.options.site', $data);
    }

    public function siteSave(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:100',
            'desc' => 'required|min:20|max:255',
            'keyword' => 'required|min:20|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.options.site')->withErrors($validator);
        }

        $init = Option::first();
        Option::where('name', $init->name)->update([
            'name' => $request->name,
            'desc' => $request->desc,
            'keyword' => $request->keyword,
        ]);

        \Session::flash('flash_message', [
            'level' => 'success',
            'subject' => 'Sukses!',
            'message' => 'Data situs telah berhasil diganti.'
        ]);

        return redirect()->route('admin.options.site');
    }

    public function verification(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'google' => 'required|max:255',
            'bing' => 'required|max:255',
            'yahoo' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.options.site')->withErrors($validator);
        }

        $init = Option::first();
        Option::where('google', $init->google)->update([
            'google' => $request->google,
            'bing' => $request->bing,
            'yahoo' => $request->yahoo,
        ]);

        \Session::flash('flash_message', [
            'level' => 'success',
            'subject' => 'Sukses!',
            'message' => 'Data situs telah berhasil diganti.'
        ]);

        return redirect()->route('admin.options.site');
    }

    public function pageIndex()
    {
        $data['page'] = Page::first();
        return view('admin.options.page', $data);
    }

    public function pageSave(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'content' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->route('admin.options.page')->withErrors($validator);
        }

        $init = Page::first();
        Page::where('judul', $init->judul)->update([
            'judul' => $request->judul,
            'content' => $request->content,
        ]);

        \Session::flash('flash_message', [
            'level' => 'success',
            'subject' => 'Sukses!',
            'message' => 'Data page telah berhasil diganti.'
        ]);

        return redirect()->route('admin.options.page');
    }
}
