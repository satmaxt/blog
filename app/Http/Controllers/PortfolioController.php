<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Label;
use App\Models\Portfolio;
use App\Http\Requests;

class PortfolioController extends Controller
{
    public function singleIndex($id, $slug)
    {
      $data['portfolio'] = Portfolio::where('id', $id)->where('slug', $slug)->first();
      $data['portfolios'] = Label::where('id', $data['portfolio']->Labels()->first()->id)->first();
      return view('guest.single-post', $data);
    }

    public function portfolioIndex()
    {
      $data['labels'] = Label::take(5)->get();
      return view('guest.indexPortfolio', $data);
    }
}
