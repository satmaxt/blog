<?php

namespace App\Http\Middleware;

use Closure;

class ManageLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $_arole = \App\User::select('role_id')->where('username', $request->user()->username)->first();
        $roles = $_arole->role_id == '1' ? 'admin' : 'member';
        if($roles == $role) {
            return $next($request);
        } else {
            return redirect()->route($roles.'.dashboard');
        }
    }
}
