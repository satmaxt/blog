<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'guest.index','uses'=>'GlobalController@homeIndex']);
Route::get('portfolio', ['as'=>'guest.portfolio.index','uses'=>'PortfolioController@portfolioIndex']);
Route::get('portfolio/{id}/{slug}', ['as'=>'guest.portfolio.show','uses'=>'PortfolioController@singleIndex']);
Route::get('contact', ['as'=>'guest.contact.index', 'uses'=>'ContactController@index']);
Route::post('contact', ['as'=>'guest.contact.store', 'uses'=>'ContactController@store']);
Route::get('about', ['as'=>'guest.about.index', 'uses'=>'GlobalController@about']);

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:admin']], function() {
	Route::get('dashboard', ['as'=>'admin.dashboard', 'uses'=>'Admin\GlobalController@dashboard']);
	Route::resource('user', 'Admin\UserController');
	Route::get('service/all', ['as'=>'admin.service.showAction', 'uses'=>'Admin\ServiceController@showAction']);
	Route::resource('teacher', 'Admin\TeacherController');
	Route::resource('service', 'Admin\ServiceController', ['except'=>['show','create']]);
	Route::resource('saids', 'Admin\SaidController');
	Route::resource('label', 'Admin\LabelController');
	Route::resource('portfolio', 'Admin\PortfolioController');


	Route::group(['prefix'=>'options'], function() {
		Route::get('user',['as'=>'admin.options.user', 'uses'=>'OptionsController@accountIndex']);
		Route::patch('user/password',['as'=>'admin.options.passSave', 'uses'=>'OptionsController@passSave']);
		Route::patch('user/info',['as'=>'admin.options.userInfo', 'uses'=>'OptionsController@userInfo']);

		// Socials
		Route::get('social', ['as'=>'admin.options.socials', 'uses'=>'OptionsController@socialIndex']);
		Route::patch('social/update', ['as'=>'admin.options.socialSave', 'uses'=>'OptionsController@socialSave']);

		// Site Options
		Route::get('site', ['as'=>'admin.options.site', 'uses'=>'OptionsController@siteIndex']);
		Route::patch('site/update', ['as'=>'admin.options.siteSave', 'uses'=>'OptionsController@siteSave']);
		Route::patch('site/verification', ['as'=>'admin.options.verification', 'uses'=>'OptionsController@verification']);

		// Page Options
		Route::get('page', ['as'=>'admin.options.page', 'uses'=>'OptionsController@pageIndex']);
		Route::patch('page/update', ['as'=>'admin.options.pageSave', 'uses'=>'OptionsController@pageSave']);
	});

});
Route::group(['prefix'=>'member', 'middleware'=>['auth', 'role:member']], function() {
  Route::resource('label', 'Admin\LabelController');
	Route::resource('portfolio', 'Admin\PortfolioController');
  Route::resource('label', 'Admin\LabelController');
});
