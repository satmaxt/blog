<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $fillable = [
    	'label',
    ];

    public $timestamps = false;

    public function Portfolios()
    {
      return $this->belongsToMany('App\Models\Portfolio');
    }
}
