<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Option extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'desc', 'keyword', 'google', 'bing', 'yahoo',
    ];

    public $timestamps = false;

}
