<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = [
    	'user_id','judul','kelompok','anggota','author','thumbnail','desc','embed','durasi','slug',
    ];

    public function Labels()
    {
      return $this->belongsToMany('App\Models\Label');
    }

    public function User()
    {
      return $this->belongsTo('App\User');
    }
}
