<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Said extends Model
{
    protected $fillable = [
    	'name', 'passion', 'content',
    ];

    public $timestamps = false;
}
