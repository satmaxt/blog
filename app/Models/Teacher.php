<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
    	'nama', 'mapel', 'foto',
    ];

    public $timestamps = false;
}
