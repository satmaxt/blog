<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'role_id' => 1,
        	'name' => 'Administrator',
        	'username' => 'admin',
        	'password' => bcrypt('faker15'),
        ]);
         App\User::create([
            'role_id' => 2,
            'name' => 'Member',
            'username' => 'member',
            'password' => bcrypt('faker15'),
        ]);
    }
}
