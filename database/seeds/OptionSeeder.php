<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Option::create([
        	'name' => 'Animasi Multimedia',
        	'desc' => 'lorem ipsum dolor sit amet constecteur',
        	'keyword' => 'lorem, ipsum, dolor, sit, amet, constecteur',
        	'google' => 'ejdwgvijIOFUWIHJkfejfijegoIHGJ3',
        	'bing' => 'ejdwgvijIOFUWIHJkfejfijegoIHGJ3',
        	'yahoo' => 'ejdwgvijIOFUWIHJkfejfijegoIHGJ3',
        ]);
    }
}
