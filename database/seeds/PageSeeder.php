<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Page::create([
        	'judul' => 'About',
        	'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui distinctio ducimus cumque rem commodi, sit autem tempora praesentium, tempore est totam incidunt dolor minus iste, sunt quis deserunt, reprehenderit laborum.',
        ]);
    }
}
