<?php

use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Social::create([
        	'facebook' => 'aji.s.aj1555',
        	'twitter' => '@satria_22_99',
        	'gplus' => '+SatriaMaxt',
        	'instagram' => 'satmaxt',
        ]);
    }
}
