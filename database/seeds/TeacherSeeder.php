<?php

use Illuminate\Database\Seeder;
use App\Models\Teacher;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');

        for($i = 1; $i<=5; $i++) {
        	Teacher::create([
        		'nama' => $faker->name,
        		'mapel' => 'Web Development',
        		'foto' => 'avatar4.png',
        	]);
        }
    }
}
