$(document).ready(function() {
    info = $('#errorss');
    $.ajaxSetup({
        headers: {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
    });
    $('#masukandata').click(function() {
        $.ajax({
            url: 'user',
            type: 'post',
            data: {
                'name': $('input[name=name]').val(), 
                'username': $('input[name=username]').val(), 
                'password': $('input[name=password]').val(),
                'role': $('select[name=role]').val(),
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                info.hide().find('ul').empty();
                if(!data.success) {
                    $.each(data.message, function(index, error) {
                        info.find('ul').append('<li>'+error+'</li>');
                    });
                    info.slideDown();
                    $('#modal-add').modal('hide');
                } else {
                    suc = $('#suc');
                    suc.find('.alert').addClass('alert-'+data.message.level);
                    suc.find('strong').append(data.message.subject);
                    suc.find('#isi').append(data.message.pesan);
                    suc.slideDown();
                    $('#modal-add').modal('hide');
                }
            },
            error: function () {
            }
        });
    });
});

$(document).ready(function() {
    prev = $('#prev-ico');
    newIco = '';
    $('#check').click(function(e) {
        e.preventDefault;
        newIco = $('#ico-val').val();
        oldIco = new Array();
        oldIco = prev.find('i').attr('class').split(' ');
        prev.find('.fa').removeClass(oldIco[2]);

        if(newIco !== "") {
            prev.find('.fa').addClass('fa-'+newIco);
        }
    });
});
$(document).ready(function() {
    obj = $('#info');
    obj2 = $('#success');
    $('#save').on('click', function(e) {
        e.preventDefault;
        $.ajaxSetup({
            headers: {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
        });
        $.ajax({
            url: 'service',
            type: 'post',
            data: {
                '_token' : $('input[name="_token"]').val(),
                'title' : $('input[name="title"]').val(),
                'desc' : $('input[name="desc"]').val(),
                'icon' : $('input[name="icon"]').val()
            },
            success: function(data) {
                if(!data.success) {
                    newObj = obj.find('ul').empty();
                    $.each(data.message, function(index, message) {
                        newObj.append('<li>'+message+'</li>');
                    });
                    obj.slideDown();
                } else {
                    obj2.find('.isi').text('');
                    obj2.find('.isi').append(data.message.pesan);
                    obj2.slideDown();
                    $('#faker').load('service/all');
                }
            }
        });
    });
});