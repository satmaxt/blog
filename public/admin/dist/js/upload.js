//  Code from devdojo.com

var form = document.getElementById('form-upload');
var request = new XMLHttpRequest();

form.addEventListener('submit', function(e){
    e.preventDefault();
    var formdata = new FormData(form);

    request.open('post', 'upload');
    request.addEventListener("load", transferComplete);
    request.send(formdata);

});

function transferComplete(data){
    obj = $('#error-upload');
    suc = $('#success-upload');

    response = JSON.parse(data.currentTarget.response);
    if(response.success){
        a = suc.find('.isi').append('Data berhasil diupload');
        suc.slideDown();
        $('#modal-thumbnail').modal('hide');
    } else {
        a = obj.find('ul').empty();
        $.each(response.message, function(index, message) {
            a.append('<li>'+message+'</li>');
        });
        obj.slideDown();
        $('#modal-thumbnail').modal('hide');
    }
}