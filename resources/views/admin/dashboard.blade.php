@extends('layouts.admin', ['_pageTitle'=>'Dashboard', '_pageDesc'=>'Welcome to admin panel'])

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-blue"><i class="fa fa-home"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Works</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-purple"><i class="fa fa-eye"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-comment"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Contact</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Subscribers</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Contact</h3>
        </div><!-- /.box-header -->
        <div class="table-responsive box-body">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Pada</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Satria Aji Putra</td>
                        <td>satriamaxt@gmail.com</td>
                        <td>22 Juli 2017</td>
                        <td><a class="btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'><i class="fa fa-eye"></i></a> <a href="" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Contact From : Satria AJi Putra</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <th>From</th>
                                    <td>Satria AJi Putra</td>
                                </tr>
                                <tr>
                                    <th>At</th>
                                    <td>22 January 2017</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>satriamaxt@gmail.com</td>
                                </tr>
                                <tr>
                                    <th>Message</th>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam magnam fugiat incidunt tempora nemo explicabo quisquam reiciendis doloribus praesentium excepturi! Possimus, dignissimos error, magni sint cumque excepturi ratione aspernatur obcaecati.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection