@extends('layouts.admin', ['_pageTitle'=>'Manage Labels', '_pageDesc'=>'Membuat, mengubah, menghapus data label disini.'])

@section('title', 'Manage Labels')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Labels</h3>
        </div>
        <div class="box-body">
            <form action="" method="POST" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input type="text" placeholder="Add new label..." name="label" class="form-control">
                        </div>
                        <div class="col-sm-2"><button id="submit" type="button" class="btn btn-block btn-primary">Submit</button></div>
                    </div>
            </form>
            @if (session()->has('flash_message.message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sukses!</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @endif
            <div id="info" style="display: none;">
                <div class="alert alert-warning">
                    <strong>Error!</strong> <br>
                    <ul></ul>
                </div>
            </div>
            <div id="success" style="display: none;">
                <div class="alert alert-success">
                    <button type="button" data-close="close" class="close">&times;</button>
                    <strong>Sukses!</strong> <br>
                    <p class="isi"></p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Label</th>
                            <th>Jumlah Post</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="faker">
                        @if (count($labels) < 1)
                            <p>Belum ada data.</p>
                        @endif
                        <?php $i = 1+($labels->perPage() * 0)+$labels->currentPage()-1; ?>
                        @foreach ($labels as $label)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $label->label }}</td>
                            <td></td>
                            <td>
                                <a href="#modal-edit" data-toggle="modal" data-url="{{ route('admin.label.edit', ['id'=>$label->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                <button data-delete="delete" data-url="{{ route('admin.label.destroy', ['id'=>$label->id]) }}" class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {!! $labels->render() !!}
        </div>
    </div>
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
@endsection
@section('footer-scripts')
<script>
    $('button[data-close="close"]').on('click', function(e) {
        e.preventDefault();
        $('#success').slideUp();
        $('#success .isi').text('');
    });

    $(document).ready(function() {
        $('a[href="#modal-edit"]').click(function(e) {
            e.preventDefault();
            $('#modal-edit .modal-content').append('<center><img src="dist/img/ajax-loader.gif" style="padding:20px auto"></center>');
            data = $(this).attr('data-url');
            $('#modal-edit .modal-content').load(data);
        });
    });

    $(document).ready(function() {
        info = $('#info');
        sukses = $('#success');

        $('#submit').on('click', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
            });

            $.ajax({
                url : 'label',
                type : 'post',
                data : {
                    'label' : $('input[name="label"]').val(),
                },
                success : function(data) {
                    if(!data.success) {
                        obj = info.find('ul').empty();
                        $.each(data.message, function(index, message) {
                            obj.append('<li>'+message+'</li>');
                        });
                        info.slideDown();
                    } else {
                        sukses.find('.isi').text(' ');
                        obj = sukses.find('.isi').append(data.message);
                        sukses.slideDown();

                        $('#faker').load('label/create');
                    }
                }
            });
        });

        $('button[data-delete="delete"]').on('click', function(e) {
            e.preventDefault();

            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(!konf) {
                return false;
            }

            data = $(this).attr('data-url');

            $.ajaxSetup({
                headers: {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
            });

            $.ajax({
                url : data,
                type : 'delete',
                success: function(data) {
                    obj = sukses.find('.isi').append(data.message);
                    sukses.slideDown();
                    $('#faker').load('label/create');
                }
            });
        });
    });
</script>
@stop
