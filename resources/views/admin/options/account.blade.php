@extends('layouts.admin', ['_pageTitle'=>'Pengaturan Akun', '_pageDesc'=>'Mengubah username, password disini'])

@section('title', 'Pengaturan Akun')

@section('content')
    @if (session()->has('flash_message.subject'))
    <div class="alert alert-{{ session()->get('flash_message.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
        {{ session()->get('flash_message.message') }}
    </div>
    @elseif(count($errors) > 0)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Ganti Info Umum</h3>
                </div>
                <div class="box-body">
                    {!! Form::model($user, ['route'=>'admin.options.userInfo', 'method'=>'patch', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <span class="col-sm-3">Nama</span>
                        <div class="col-sm-9">
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Username</span>
                        <div class="col-sm-9">
                            {!! Form::text('username', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Password</span>
                        <div class="col-sm-9">
                            {!! Form::password('password', ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Ganti Password</h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['route'=>'admin.options.passSave', 'method'=>'patch', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <span class="col-sm-3">Password lama</span>
                        <div class="col-sm-9">
                            {!! Form::password('oldPass', ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Password baru</span>
                        <div class="col-sm-9">
                            {!! Form::password('newPass', ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Masukan ulang pasword</span>
                        <div class="col-sm-9">
                            {!! Form::password('newPass_confirmation', ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
