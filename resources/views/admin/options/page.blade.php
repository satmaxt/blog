@extends('layouts.admin', ['_pageTitle'=>'Halaman Tentang', '_pageDesc'=>'Mengubah deskripsi halaman tentang'])

@section('title', 'Halaman Tentang')

@section('content')
    @if (session()->has('flash_message.subject'))
    <div class="alert alert-{{ session()->get('flash_message.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
        {{ session()->get('flash_message.message') }}
    </div>
    @elseif(count($errors) > 0)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Halaman Tentang</h3>
        </div>
        <div class="box-body">
            {!! Form::model($page, ['route'=>'admin.options.pageSave', 'method'=>'patch']) !!}
            <div class="form-group">
                <label class="control-label">Judul</label>
                <div class="row">
                    <div class="col-sm-10">{!! Form::text('judul', null, ['class'=>'form-control']) !!}</div>
                    <div class="col-sm-2"><button class="btn btn-primary btn-block">Simpan</button></div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('admin/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      height: 300,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });
</script>
@stop
