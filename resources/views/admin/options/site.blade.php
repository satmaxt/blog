@extends('layouts.admin', ['_pageTitle'=>'Pengaturan Situs', '_pageDesc'=>'Mengubah pengaturan situs disini'])

@section('title', 'Pengaturan Situs')

@section('content')
    @if (session()->has('flash_message.subject'))
    <div class="alert alert-{{ session()->get('flash_message.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
        {{ session()->get('flash_message.message') }}
    </div>
    @elseif(count($errors) > 0)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengaturan Situs</h3>
                </div>
                <div class="box-body">
                    {!! Form::model($site, ['route'=>'admin.options.siteSave', 'method'=>'patch', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <span class="col-sm-3">Nama Situs</span>
                        <div class="col-sm-9">
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Deskripsi Situs</span>
                        <div class="col-sm-9">
                            {!! Form::text('desc', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Keyword Situs</span>
                        <div class="col-sm-9">
                            {!! Form::text('keyword', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Verifikasi Situs</h3>
                </div>
                <div class="box-body">
                    {!! Form::model($site, ['route'=>'admin.options.verification', 'method'=>'patch', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <span class="col-sm-3">Google Verification</span>
                        <div class="col-sm-9">
                            {!! Form::text('google', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Bing Verification</span>
                        <div class="col-sm-9">
                            {!! Form::text('bing', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Yahoo Verification</span>
                        <div class="col-sm-9">
                            {!! Form::text('yahoo', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
