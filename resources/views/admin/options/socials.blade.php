@extends('layouts.admin', ['_pageTitle'=>'Pengaturan Sosial Media', '_pageDesc'=>'Mengubah username, sosial media disini'])

@section('title', 'Pengaturan Sosial Media')

@section('content')
    @if (session()->has('flash_message.subject'))
    <div class="alert alert-{{ session()->get('flash_message.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
        {{ session()->get('flash_message.message') }}
    </div>
    @elseif(count($errors) > 0)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Ganti username sosial media</h3>
                </div>
                <div class="box-body">
                    {!! Form::model($social, ['route'=>'admin.options.socialSave', 'method'=>'patch', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <span class="col-sm-3">Facebook</span>
                        <div class="col-sm-9">
                            {!! Form::text('facebook', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Google Plus</span>
                        <div class="col-sm-9">
                            {!! Form::text('gplus', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Twitter</span>
                        <div class="col-sm-9">
                            {!! Form::text('twitter', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-3">Instagram</span>
                        <div class="col-sm-9">
                            {!! Form::text('instagram', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
