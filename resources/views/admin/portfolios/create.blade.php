@extends('layouts.admin', ['_pageTitle'=>'Tambah Portfolio', '_pageDesc'=>'Menambahkan portfolio baru'])

@section('title', 'Tambah Portfolio')

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.css') }}">
<style type="text/css">
    .widgets:first-child {
        padding-top: 0;
    }
    .widgets {
        padding: 20px 0;
        border-bottom: 1px solid #f0f0f0;
    }
    .widgets>h3 {
        padding: 0;
        margin: 0px 0px 15px 0px;
    }
</style>
@endsection

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Portfolio</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            @if (session()->has('flash_message.subject'))
            <div class="alert alert-{{ session()->get('flash_message.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @elseif(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif

            {!! Form::open(['route'=>'admin.portfolio.store', 'id'=>'formmm', 'files'=>true]) !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-10">
                            {!! Form::text('judul', null, ['class'=>'form-control','placeholder'=>'Judul portfolio']) !!}
                        </div>
                        <div class="col-sm-2"><button type="submit" class="btn btn-primary btn-block">Save</button></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">{!! Form::textarea('embed', null, ['class'=>'form-control', 'id'=>'video', 'style'=>'display:none;', 'placeholder'=>'Video embed here.']) !!}</div>
                        <div class="form-group">
                            {!! Form::textarea('desc', null, ['class'=>'form-control', 'id'=>'arti']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="widgets">
                            <h3>Thumbnail</h3>
                            <div class="form-group">
                              <input type="file" class="form-control" name="thumbnail">
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Template</h3>
                            <div class="form-group">
                                <select id="template" class="form-control">
                                    <option value selected>Pilih template (wajib)</option>
                                    <option value="1">Video</option>
                                    <option value="2">Artikel</option>
                                </select>
                            </div>
                            <div class="form-group" id="durasi" style="display: none;">
                                <label class="control-label">Durasi Video</label>
                                {!! Form::text('durasi', null, ['class'=>'form-control','placeholder'=>'Kosongkan jika tidak ada']) !!}
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Kategori</h3>
                            <div class="form-group">
                                <select class="selectpicker" name="label" data-live-search="true">
                                  <option value selected>Pilih kategori -</option>
                                  @foreach ($labels as $label)
                                    <option value="{{ $label->id }}">{{ $label->label }}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Pengerjaan</h3>
                            <div class="form-group">
                                <select id="kelompok" class="form-control">
                                    <option value selected>Select metode -</option>
                                    <option value="1">Berkelompok</option>
                                    <option value="2">Individu</option>
                                </select>
                            </div>
                            <div class="form-group" id="selkelompok" style="display: none;">
                                <input type="text" name="namakelompok" placeholder="Nama kelompok" class="form-control">
                            </div>
                            <div class="form-group" id="anggota" style="display: none;">
                                <input type="text" name="anggota" placeholder="Anggota" class="form-control">
                                <span class="help-block">Gunakan ';' untuk pemisah anggota. Misalkan jhon;doe;samantha</span>
                            </div>
                            <div class="form-group" id="individu" style="display: none;">
                                <input type="text" class="form-control" name="namaindividu" placeholder="Nama anda">
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('admin/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
      selector: 'textarea#arti',
      height: 400,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });

    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
          size: 10,
          width: '100%',
        });
    });

    $(document).ready(function() {
        method = $('#kelompok');
        method.on('change', function(e) {
            e.preventDefault();
            if(method.val() == "1") {
                $('#selkelompok').slideDown();
                $('#anggota').slideDown();
                $('#individu').slideUp();
                $('input[name="namaindividu"]').val('');
            } else if (method.val() == "2") {
                $('#selkelompok').slideUp();
                $('#anggota').slideUp();
                $('input[name="namakelompok"]').val('');
                $('input[name="anggota"]').val('');
                $('#individu').slideDown();
            }
        });
    });
    $(document).ready(function() {
        element = $('#template');
        element.on('change', function() {
            if(element.val() == "1") {
                $('#video').slideDown();
                $('#durasi').slideDown();
            } else if (element.val() == "2") {
                $('#video').slideUp();
                $('#durasi').slideUp();
                $('input[name="durasi"]').val('');
                $('textarea[name="embed"]').val('');
            }
        });
    });
</script>
@stop;
