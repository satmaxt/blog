@extends('layouts.admin', ['_pageTitle'=>'Manage Saids Data', '_pageDesc'=>'Membuat, mengubah, menghapus data kat mereka disini.'])

@section('title', 'Manage Saids Data')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Saids Data</h3>
            <a data-toggle="modal" href='#modal-tambah' class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Tambah</a>
        </div>
        <div class="box-body">
            @if (session()->has('flash_message.message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sukses!</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @elseif(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
            <div id="info" style="display: none;">
                <div class="alert alert-warning">
                    <strong>Error!</strong> <br>
                    <ul></ul>
                </div>
            </div>
            <div id="success" style="display: none;">
                <div class="alert alert-success">
                    <button type="button" data-close="close" class="close">&times;</button>
                    <strong>Sukses!</strong> <br>
                    <p class="isi"></p>                    
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>As</th>
                            <th>Content</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="result-faker">
                        <?php $i = 1+($saids->perPage() * 0)+$saids->currentPage()-1; ?>
                        @foreach ($saids as $said)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $said->name }}</td>
                            <td>{{ $said->passion }}</td>
                            <td>{{ str_limit($said->content, 50) }}</td>
                            <td><a data-toggle="modal" href='#modal-edit' data-url="{{ route('admin.saids.edit',['id'=>$said->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                            {!! Form::open(['url'=>route('admin.saids.destroy', ['id'=>$said->id]), 'method'=>'delete', 'style'=>'display:inline-block !important','class'=>'form-delete']) !!}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>'admin.saids.store', 'class'=>'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Sebagai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="passion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Content</label>
                        <div class="col-sm-10">
                            {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="simpan">Save changes</button>
                </div>
                {!! Form::close() !!}   
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                  
            </div>
        </div>
    </div>
@endsection
@section('footer-scripts')
<script src="{{ asset('admin/dist/js/site.js') }}"></script>
<script>
    $('button[data-close="close"]').on('click', function(e) {
        e.preventDefault;
        $('#success').slideUp();
        $('#success .isi').text('');
    });
    $(document).ready(function() {
        info = $('#info');
        sukses = $('#success');
        $('#simpan').click(function(e) {
            e.preventDefault;
            $.ajaxSetup({
                headers : {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
            });

            $.ajax({
                url : 'saids',
                type : 'post',
                data : {
                    'name' : $('input[name="name"]').val(),
                    'passion' : $('input[name="passion"]').val(),
                    'content' : $('textarea[name="content"]').val(),
                },
                success : function(data) {
                    if(!data.success) {
                        obj = info.find('ul').empty();
                        $.each(data.message, function(index, message) {
                            obj.append('<li>'+message+'</li>');
                        });
                        info.slideDown();
                        $('#modal-tambah').modal('hide');
                    } else {
                        $('#result-faker').load('saids/create')
                        obj = sukses.find('.isi');
                        obj.append(data.message);
                        sukses.slideDown();
                        $('#modal-tambah').modal('hide');
                    }
                }
            });
        });
    });
    $(document).ready(function() {
        $('a[href="#modal-edit"]').click(function(e) {
            e.preventDefault;
            data = $(this).attr('data-url');
            $('#modal-edit .modal-content').load(data);
        });
    });
    $(document).ready(function() {
        $('.form-delete').on('submit', function() {
            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(konf) {
                return konf;
            } else {
                return false;
            }
        });
    });
</script>
@stop
