@extends('layouts.admin', ['_pageTitle'=>'Edit Services', '_pageDesc'=>'Membuat, mengubah, menghapus data service disini.'])

@section('title', 'Edit Services')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Services</h3>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    {!! Form::model($service, ['url'=>route('admin.service.update',['id'=>$service->id]), 'class'=>'form-horizontal', 'method'=>'patch']) !!}
                        <div class="form-group">
                            <label class="control-label col-sm-2">Title</label>
                            <div class="col-sm-10">
                                {!! Form::text('title', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Description</label>
                            <div class="col-sm-10">
                                {!! Form::text('desc', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Icon</label>
                            <div class="col-sm-8">
                                {!! Form::text('icon', null, ['class'=>'form-control', 'id'=>'ico-val', 'placeholder'=>'Ex: home']) !!}
                                <span class="help-block">Insert icon name, see the name from <a href="http://fontawesome.io/icons/" target="_blank">Here.</a></span>
                            </div>
                            <div class="col-sm-2"><button type="button" class="btn btn-success" id="check">Check</button></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8 col-sm-offset-2">
                            <span class="help-block">Preview Icon</span>
                            <a id="prev-ico" class="thumbnail" style="text-align: center;padding: 20px 0;">
                                <i class="fa fa-4x{{ " fa-".$service->icon }}"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-scripts')
<script src="{{ asset('admin/dist/js/site.js') }}"></script>
@stop
