@extends('layouts.admin', ['_pageTitle'=>'Manage Services', '_pageDesc'=>'Membuat, mengubah, menghapus data service disini.'])

@section('title', 'Manage Services')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Services</h3>
            <a data-toggle="modal" href='#modal-guru' class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Tambah</a>
        </div>
        <div class="box-body">
            @if (session()->has('flash_message.message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sukses!</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @endif
            <div id="info" style="display: none;">
                <div class="alert alert-warning">
                    <button type="button" aria-hidden="true">&times;</button>
                    <strong>Error!</strong> <br>
                    <ul></ul>
                </div>
            </div>
            <div id="success" style="display: none;">
                <div class="alert alert-success">
                    <button type="button" data-close="close" class="close">&times;</button>
                    <strong>Sukses!</strong> <br>
                    <p class="isi"></p>                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {!! Form::open(['route'=>'admin.service.store', 'class'=>'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="control-label col-sm-2">Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="desc">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Icon</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="ico-val" name="icon" placeholder="Ex: home">
                                <span class="help-block">Insert icon name, see the name from <a href="http://fontawesome.io/icons/" target="_blank">Here.</a></span>
                            </div>
                            <div class="col-sm-2"><button type="button" class="btn btn-success" id="check">Check</button></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-primary" type="button" id="save">Save</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8 col-sm-offset-2">
                            <span class="help-block">Preview Icon</span>
                            <a id="prev-ico" class="thumbnail" style="text-align: center;padding: 20px 0;">
                                <i class="fa fa-4x"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Icon</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="faker">
                        <?php $i = 1+($services->perPage() * 0)+$services->currentPage()-1; ?>
                        @foreach ($services as $service)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $service->title }}</td>
                            <td>{{ $service->desc }}</td>
                            <td><i class="fa fa-2x fa-{{ $service->icon }}"></i></td>
                            <td>
                                <a href="{{ route('admin.service.edit', ['id'=>$service->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                {!! Form::open(['url'=>route('admin.service.destroy', ['id'=>$service->id]),'method'=>'delete', 'style'=>'display:inline-block !important', 'class'=>'form-delete']) !!}
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {!! $services->render() !!}
        </div>
    </div>
@endsection
@section('footer-scripts')
<script src="{{ asset('admin/dist/js/site.js') }}"></script>
<script>
    $('button[data-close="close"]').on('click', function(e) {
        e.preventDefault;
        $('#success').slideUp();
        $('#success .isi').text('');
    });

    $(document).ready(function() {
        $('.form-delete').on('submit', function() {
            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(konf) {
                return konf;
            } else {
                return false;
            }
        });
    });
</script>
@stop
