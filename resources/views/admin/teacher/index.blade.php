@extends('layouts.admin', ['_pageTitle'=>'Manage Teachers', '_pageDesc'=>'Membuat, mengubah, menghapus data guru disini.'])

@section('title', 'Manage Teachers')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Teacher</h3>
            <a data-toggle="modal" href='#modal-guru' class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Tambah</a>
        </div>
        <div class="box-body">
            @if (session()->has('flash_message.subject'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @elseif(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                @if (count($teachers) === 0)
                    <center>No data found.</center>
                @endif
                @foreach ($teachers as $teacher)
                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="{{ asset('image/'.$teacher->foto) }}" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $teacher->nama }}</h3>
                          <p class="text-muted text-center">{{ $teacher->mapel }}</p>
                          <div style="text-align: center;">
                            <a data-toggle="modal" href='#modal-edit' data-url="{{ route('admin.teacher.edit', ['id'=>$teacher->id]) }}" class="btn btn-primary"><b>Edit</b></a>
                            {!! Form::open(['url'=>route('admin.teacher.destroy', ['id'=>$teacher->id]), 'method'=>'delete', 'style'=>'display:inline-block !important', 'class'=>'form-delete']) !!}
                                <button type="submit" class="btn btn-danger">Delete</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-guru">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>'admin.teacher.store', 'files'=>true]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Guru</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>Nama</th>
                                    <td><input type="text" class="form-control" name="nama"></td>
                                </tr>
                                <tr>
                                    <th>Pelajaran</th>
                                    <td><input type="text" class="form-control" name="mapel"></td>
                                </tr>
                                <tr>
                                    <th>Foto</th>
                                    <td><input type="file" id="datafoto" class="form-control" name="foto"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="add-guru" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
               
            </div>
        </div>
    </div>
@endsection
@section('footer-scripts')
<script>
    $(document).ready(function() {
        $('a[href="#modal-edit"]').click(function(e) {
            e.preventDefault();
            target = $(this).attr('data-url');
            $('#modal-edit .modal-content').load(target);
        });
    });
    $(document).ready(function() {
        $('.form-delete').on('submit', function() {
            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(konf) {
                return konf;
            } else {
                return false;
            }
        });
    });
</script>
@stop
