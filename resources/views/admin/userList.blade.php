
@extends('layouts.admin', ['_pageTitle'=>'Manage Users', '_pageDesc'=>'Manage user account'])

@section('title', 'Manage Users')

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Users</h3>
            <a data-toggle="modal" href='#modal-add' class="pull-right btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Tambah User</a>
        </div><!-- /.box-header -->
        <div class="table-responsive box-body">
            @if (count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @elseif (session()->has('flash_message.level'))
            <div class="alert alert-{{ session()->get('flash_message.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
                {{ session()->get('flash_message.pesan') }}
            </div>
            @endif
            <div id="errorss" style="display: none;">
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Error!</strong>
                    <ul>
                        
                    </ul>
                </div>
            </div>
            <div id="suc" style="display: none;">
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong></strong>
                    <div id="isi"></div>
                </div>
            </div>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 + ($users->currentPage() - 1) * $users->perPage(); ?>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td><span class="label label-primary">{{ $user->role_id == 1 ? 'Admin' : 'Member' }}</span></td>
                        <td>{{ date('d M Y', strtotime($user->created_at)) }}</td>
                        <td><a class="btn btn-primary btn-sm" data-url="{{ route('admin.user.edit', ['id'=>$user->id]) }}" data-toggle="modal" href='#modal-edit'><i class="fa fa-pencil"></i></a> <a href="" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $users->render() !!}
        </div>
    </div>
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah User</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            {{ csrf_field() }}
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <td><input type="text" name="name" class="form-control"></td>
                                </tr>
                                <tr>
                                    <th>Username</th>
                                    <td><input type="text" name="username" class="form-control"></td>
                                </tr>
                                <tr>
                                    <th>Password</th>
                                    <td><input type="password" name="password" class="form-control"></td>
                                </tr>
                                <tr>
                                    <th>Role</th>
                                    <td><select name="role" class="form-control">
                                        <option value selected>Select role -</option>
                                        <option value="1">Admin</option>
                                        <option value="2">Member</option>
                                    </select></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="masukandata" class="btn btn-primary">Save data</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('admin/dist/js/site.js') }}"></script>
<script>
    $(document).ready(function() {
        $('a[href="#modal-edit"]').click(function(e) {
            e.preventDefault();
            target = $(this).attr('data-url');
            $('#modal-edit .modal-content').load(target);
        });
    });
    $(document).ready(function() {
        $('.form-delete').on('submit', function() {
            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(konf) {
                return konf;
            } else {
                return false;
            }
        });
    });
</script>
@stop