@extends('layouts.guest')

@section('title','About Us')

@section('content')
  <div id="aboutUS">
        <div class="container">
              <div class="aboutUS-title title">
                    <h2>{{ $about->title }}</h2>
               </div>
               <div class="aboutUS-title-bottom">
                    {!! $about->content !!}
               </div>
               <div class="aboutUS-teacher">
                    <div class="aboutUS-teacher-title title">
                        <h2>About Teacher</h2>
                   </div>
                   <div class="aboutUS-teacher-inner">
                         <div class="row">
                           @foreach ($teachers as $teacher)
                           <div class="col-md-4">
                                  <div class="teacher-content">
                                         <div class="teacher-img">
                                               <img src="{{ url('image/'.$teacher->foto) }}">
                                         </div>
                                         <div class="teacher-about">
                                               <h4>{{ $teacher->nama }}</h4>
                                               <p>{{ $teacher->mapel }}</p>
                                         </div>
                                  </div>
                           </div>
                           @endforeach
                         </div>
                   </div>
               </div>
        </div>
  </div>

    <div id="sletter">
          <div class="sletter-blur">
                <div class="container">
                     <div class="sletter-inner">
                            <h2 style="font-family:Bold;">Kunjungi kami di SMK NEGERI 1 CIBADAK</h2>
                     </div>
              </div>
          </div>
  </div>
@stop
