@extends('layouts.guest')

@section('title','Contact Us')

@section('content')
  <div id="contactUS">
        <div class="container">
               <div class="contactUS-title title">
                    <h2>Contact Us</h2>
               </div>
               <div class="contactUS-title-bottom">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
               </div>
               <div class="map bottom">
                    <div class="map-title">
                          <h3>GET IN TOUCH</h3>
                    </div>
                    <div id="map" class="map-inner">
                           {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127640.75918330808!2d103.8466694772479!3d1.3111268075660079!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da11238a8b9375%3A0x887869cf52abf5c4!2sSingapore!5e0!3m2!1sen!2sin!4v1436965675589"> </iframe> --}}
                    </div>
               </div>
               <div class="contactUS-inner bottom">
                     <div class="contactUs-title">
                           <h3>CONTACT INFO</h3>
                     </div>
                     <div class="contactUs-title-bottom">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi</p>
                     </div>
                     <div class="contactUS-inner-content">
                            @if(count($errors) > 0)
                              <div class="alert alert-warning">
                                <b>Errors!</b>
                                <ul>
                                  @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                              </div>
                            @elseif(session()->has('flash_message.message'))
                              <div class="alert alert-success">
                                <b>Sukses!</b>
                                {{ session()->get('flash_message.message') }}
                              </div>
                            @endif
                           {!! Form::open(['route'=>'guest.contact.store', 'method'=>'post']) !!}
                                 <div class="row">
                                     <div class="col-md-4">
                                           <div class="thumbnail">
                                               <input type="text" placeholder="Name" name="name" required>
                                           </div>
                                     </div>
                                     <div class="col-md-4">
                                           <div class="thumbnail">
                                                <input type="email" placeholder="Email" name="email">
                                           </div>
                                     </div>
                                     <div class="col-md-4">
                                           <div class="thumbnail">
                                                 <input type="text" placeholder="Telephone" name="telephone" required>
                                           </div>
                                     </div>
                                     <div class="col-md-12">
                                           <div class="thumbnail">
                                                  <textarea placeholder="Message" name="message" required></textarea>
                                           </div>
                                     </div>
                                     <button type="submit" name="submit">Submit</button>
                                  </div>
                           {!! Form::close() !!}
                     </div>
               </div>
        </div>
  </div>
@endsection

@section('footer-scripts')
  <script>
    function initMap() {
      var myLatLng = {lat: -6.8946619, lng: 106.8146309};

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
      });
    }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDGSKsKGEQkB3wGfx7Devn-m_31Yo1fF4&signed_in=true&callback=initMap"></script>
@stop
