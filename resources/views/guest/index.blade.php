@extends('layouts.guest')

@section('title','Welcome')

@section('header')
  <header class="header-content" style="text-align:center;">
        <div class="container">
                <div class="header-content-container">
                     <div class="header-content-title">
                           <h1 style="font-family:Bold;color:#fff;font-size:50px;">Mari Berkarya Bersama Kami</h1>
                     </div>
                     <div class="header-content-inner">
                           <p style="color:#fff;font-size:18px;">Buat impianmu menjadi kenyataan</p><br>
                     </div>
                </div>
                <div class="header-content-scroll">
                     <a href="#about" data-uk-smooth-scroll="{offset: 90}"><i class="fa fa-long-arrow-down"></i></a>
                </div>
            </div>
  </header>
@endsection

@section('content')
  <div id="about">
    <div class="container">
      <div class="about-title title">
            <h2>{!! $about->judul !!}</h2>
      </div>
      <div class="about-content" style="font-size:15px;">
            {!! $about->content !!}
      </div>
      <div class="about-follow">
           <ul>
               <li><a href=""><i class="fa fa-facebook"></i></a></li>
               <li><a href=""><i class="fa fa-google-plus"></i></a></li>
               <li><a href=""><i class="fa fa-twitter"></i></a></li>
               <li><a href=""><i class="fa fa-instagram"></i></a></li>
           </ul>
      </div>
    </div>
    <style type="text/css">
     @media screen and (max-width: 1180px){
         #about{
            background-position: -150px 0px;
         }
     }

     @media screen and (max-width: 1024px){
         #about{
            background-position: -200px 0px;
         }
     }
     </style>
  </div>

  <div id="service">
   <div class="container">
         <div class="service-title title">
               <h2>Our Service</h2>
         </div>
         <div class="service-content">
           <div class="row">
             @foreach ($services as $service)
               <div class="col-md-4">
                     <div class="service-col">
                           <i class="fa fa-{{ $service->icon }}"></i>
                           <h3>{{ $service->title }}</h3>
                           <p>{{ $service->desc }}</p>
                     </div>
               </div>
             @endforeach
           </div>
         </div>
   </div>
  </div>

  <div id="work">
           <div class="container">
                 <div class="work-title content-title title">
                       <h2>Our Work</h2>
                 </div>
                 <div class="work-content content-ul">
                           <ul id="filter" class="uk-subnav uk-subnav-pill" style="font-size:15px;">
                                <li class="uk-active" data-uk-filter=""><a href="#">All</a></li>
                                @foreach ($labels as $label)
                                  <li data-uk-filter="{{ str_slug($label->label, '-') }}"><a href="#">{{ $label->label }}</a></li>
                                @endforeach
                            </ul>
                            <div class="row" id="work-col content-col" data-uk-grid="{gutter: 20, controls: '#filter'}">
                                @foreach ($labels as $label)
                                  @foreach ($label->Portfolios()->get() as $portfolio)
                                <div class="col-sm-4 col-md-3" data-uk-filter="{{ str_slug($label->label, '-') }}">
                                      <div class="work-content-inner content-inner">
                                            <img src="image/{{ $portfolio->thumbnail }}">
                                            <a href="{!! route('guest.portfolio.show', ['id'=>$portfolio->id, 'slug'=>$portfolio->slug]) !!}"><div class="work-hover content-hover">
                                                  <i class="fa fa-plus"></i>
                                            </div></a>
                                      </div>
                                </div>
                                  @endforeach
                                @endforeach
                            </div>
                 </div>
           </div>
  </div>

  <div id="testimonials">
        <div class="container">
          <div class="testimonials-title title">
                <h2>Testimonials</h2>
          </div>
          <div class="carousel slide" id="screenshot-carousel" data-ride="carousel">
            <div class="carousel slide" id="screenshot-carousel" data-ride="carousel">
              <div class="carousel-inner">
                <?php $x = 1 ?>
                @foreach ($saids as $said)
                  <?php $x++ ?>
                  <div class="item {{ $x > 2 ? '' : 'active' }}">
                    <div class="testimonials-content-col">
                      <div class="testimonials-inner">
                          <ul>
                            <li><img src="asset/img/defaultUsers.png" style="width:100%;border-radius:100px;border:1px solid #e0e0e0;"></li>
                            <li style="text-align:left !important;">
                              <h4>{{ $said->name }}</h4>
                              <p><b>{{ $said->passion }}</b></p>
                            </li>
                          </ul>
                            <p>"{{ $said->content }}"</p>
                      </div>
                    </div>
                  </div>
                @endforeach
             </div>
           </div>
           <a class="left carousel-control" href="#screenshot-carousel" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
           </a>
           <a class="right carousel-control" href="#screenshot-carousel" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
           </a>
          </div>
        </div>
  </div>
  <div id="sletter">
          <div class="sletter-blur">
                <div class="container">
                     <div class="sletter-content">
                            <form action="" method="post">
                                <input type="text" placeholder="News letter" name="sletter">
                                <button type="submit" name="submit"><i class="fa fa-paper-plane"></i></button>
                                <div class="clearfix"></div>
                            </form>
                     </div>
                     <div class="sletter-inner">
                            <h2 style="font-family:Bold;">Kunjungi kami di SMK NEGERI 1 CIBADAK</h2>
                            <p style="margin-top:10px;"></p>
                     </div>
              </div>
          </div>
  </div>
@stop
