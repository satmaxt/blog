@extends('layouts.guest')

@section('title','Portfolio')

@section('content')
  <div id="ourWork">
          <div class="container">
                   <div class="ourWork-title content-title title">
                         <h2>Our Work</h2>
                   </div>
                   <div class="content-p">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure</p>
                   </div>
                   <div class="row">
                   <div class="ourWork-content content-ul">
                             <ul id="filter" class="uk-subnav uk-subnav-pill" style="font-size:15px;">
                                  <li class="uk-active" data-uk-filter=""><a href="#">All</a></li>
                                  @foreach ($labels as $label)
                                    <li data-uk-filter="{{ str_slug($label->label, '-') }}"><a href="#">{{ $label->label }}</a></li>
                                  @endforeach
                              </ul>
                              <div class="row" id="ourWork-col content-col" data-uk-grid="{gutter: 20, controls: '#filter'}">
                                  @foreach ($labels as $label)
                                    @foreach ($label->Portfolios()->take(10)->get() as $portfolio)
                                      <div class="col-sm-6 col-md-3" data-uk-filter="{{ str_slug($label->label, '-') }}">
                                            <div class="ourWork-content-inner content-inner">
                                                  <img src="{{ url('image/'.$portfolio->thumbnail) }}">
                                                  <a href="{!! route('guest.portfolio.show', ['id'=>$portfolio->id, 'slug'=>$portfolio->slug]) !!}"><div class="ourWork-hover content-hover">
                                                        <i class="fa fa-plus"></i>
                                                  </div></a>
                                            </div>
                                      </div>
                                    @endforeach
                                  @endforeach
                              </div>
                   </div>
                   </div>
             </div>
    </div>

      <div id="sletter">
            <div class="sletter-blur">
                  <div class="container">
                       <div class="sletter-inner">
                              <h2 style="font-family:Bold;">Kunjungi kami di SMK NEGERI 1 CIBADAK</h2>
                       </div>
                </div>
            </div>
    </div>

@stop
