@extends('layouts.guest')

@section('title',$portfolio->judul.' - Portfolio')

@section('content')
  <div id="single-post">
    <div class="container">
     <div class="single-inner">
       <div class="row">
          <div class="col-md-6">
               <div class="single-inner-img">
                    <img src="{{ url('image/'.$portfolio->thumbnail) }}">
               </div>
          </div>
          <div class="col-md-6">
             <div class="single-inner-post">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
                    <li role="presentation"><a href="#tentang" aria-controls="tentang" role="tab" data-toggle="tab">About</a></li>
                  </ul>

                  <!-------  tab panes -------->

                  <div class="tab-content">
                       <div class="tab-pane active" role="tabpanel" id="detail" style="color:#7a7a7a !important;">
                           <ul>
                              <li>Judul :</li>
                              <li>{{ $portfolio->judul }}</li>
                           </ul>
                           <ul>
                              <li>Kategori :</li>
                              <li>{{ $portfolio->Labels()->first()->label }}</li>
                           </ul>
                           <ul>
                              <li>Durasi :</li>
                              <li>{{ $portfolio->durasi == '' ? '-' : $portfolio->durasi }}</li>
                           </ul>
                           @if ($portfolio->kelompok == "")
                             <ul>
                                <li>Oleh :</li>
                                <li>{{ $portfolio->author }}</li>
                             </ul>
                           @else
                             <ul>
                                <li>Kelompok :</li>
                                <li>{{ $portfolio->kelompok }}</li>
                             </ul>
                             <ul>
                                <li>Anggota Kelompok :</li>
                                <li>{{ str_repleace(';', ', ', $portfolio->anggota) }}</li>
                             </ul>
                           @endif
                       </div>
                       <div class="tab-pane" role="tabpanel" id="tentang" style="color:#7a7a7a !important;">
                           {!! $portfolio->desc !!}
                       </div>
                  </div>
             </div>
          </div>
       </div>
     </div>
     <div class="single-more">
          <div class="single-more-title">
              <h3 style="font-family:Light;">Lainnya</h3>
          </div>
           <div class="row" id="single-more-row">
             @foreach ($portfolios->Portfolios()->get() as $item)
               <div class="col-sm-4 col-md-3">
                     <div class="single-content-inner content-inner">
                           <img src="{{ url('image/'.$item->thumbnail) }}">
                           <a href="{!! route('guest.portfolio.show',['id'=>$item->id, 'slug'=>$item->slug]) !!}"><div class="single-hover content-hover">
                                 <i class="fa fa-plus"></i>
                           </div></a>
                     </div>
               </div>
             @endforeach
           </div>
     </div>
    </div>
  </div>

@stop
