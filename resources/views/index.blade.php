<form action="upload" id="upload">
	{!! csrf_field() !!}
    <input type="file" name="file[]" multiple><br />
    <input type="submit">
</form>
<img src="{{ Storage::url('Screenshot_2.png') }}" alt="">
<div id="message"></div>

<script>
var form = document.getElementById('upload');
var request = new XMLHttpRequest();

form.addEventListener('submit', function(e){
    e.preventDefault();
    // var formdata = new FormData(form);
    var formdata = new FormData(form);

    request.open('post', 'upload');
    request.addEventListener("load", transferComplete);
    request.send(formdata);

});

function transferComplete(data){
    response = JSON.parse(data.currentTarget.response);
    if(response.success){
        document.getElementById('message').innerHTML = "Successfully Uploaded Files!";
    }
}

$('#upload').click(function(e) {
    e.preventDefault();
    request = new XMLHttpRequest();
    formdata = $('#files').val();
    request.open('POST', 'portfolio/upload');
    request.setRequestHeader('X-CSRF-Token', $('meta[name="csrf_token"]').attr('content'));
    request.send(formdata);
});
</script>