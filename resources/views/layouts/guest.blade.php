<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="theme-color" content="#">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{!! asset('theme/asset/css/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="{!! asset('theme/asset/css/bootstrap-theme.min.css') !!}" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{!! asset('theme/asset/css/ie10-viewport-bug-workaround.css') !!}" rel="stylesheet">
    <!-- Style tambahan -->
    <link rel="stylesheet" href="{!! asset('theme/style.css') !!}">
    <!------ Uikit ------>
    <script src="{!! asset('theme/asset/uikit/jquery.js') !!}"></script>
    <script src="{!! asset('theme/asset/uikit/uikit.js') !!}"></script>
    <script src="{!! asset('theme/asset/uikit/grid.js') !!}"></script>
    <!-- Font-Awesome -->
    <link href="{!! asset('theme/asset/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{!! asset('theme/asset/js/ie-emulation-modes-warning.js') !!}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('header-scripts')

</head>
<body>

  <div id="header">
      <div class="header-blur">
              <nav class="navbar-top">
                    <div class="container">
                            <div class="navbar">
                                  <div class="navbar-header">
                                       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <i style="color:#fff;" class="fa fa-navicon"></i>
                                      </button>
                                      <div class="logo">
                                            <div class="header-img">
                                                  <!------<a href=""><img src="asset/img/logo.png"></a>---->
                                                  <h1 style="font-family:Bold;color:#fff;top:10px;position:absolute;">LOGO</h1>
                                            </div>
                                      </div>
                                  </div>
                                  <div id="navbar" class="navbar-collapse collapse">
                                        <ul class="nav navbar-nav navbar-right" style="font-size:15px;">
                                             <li><a href="" class="active">Home</a></li>
                                             <li><a href="">About Us</a></li>
                                             <li><a href="">Our Work</a></li>
                                             <li><a href="">Contact Us</a></li>
                                        </ul>
                                        <style>
                                        @media (min-width: 768px){
                                          .navbar-nav>li>a{
                                            padding-top: 0px;
                                            padding-bottom: 0px;
                                          }
                                        }
                                        </style>
                                  </div>
                          </div>
                    </div>
              </nav>
              @yield('header')
        </div>
  </div>

  @yield('content')

  <footer id="footer">
           <div class="container">
                  <div class="footer-content" style="font-size:15px;">
                        <div class="row">
                              <div class="col-md-6">
                                  <p>© 2016 website, All rights reserved | Creat By <a href="">Saint Studio</a></p>
                              </div>
                              <div class="col-md-6">
                                   <ul class="navbar-bottom">
                                         <li><a href="" class="active">Home</a></li>
                                         <li><a href="">About Us</a></li>
                                         <li><a href="">Our Work</a></li>
                                         <li><a href="">Contact Us</a></li>
                                    </ul>
                              </div>
                         </div>
                  </div>
           </div>
  </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="asset/js/jquery.min.js"><\/script>')</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{!! asset('theme/asset/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('theme/asset/js/docs.min.js') !!}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{!! asset('theme/asset/js/ie10-viewport-bug-workaround.js') !!}"></script>

    @yield('footer-scripts')

</body>
</html>
