@extends('layouts.admin', ['_pageTitle'=>'Dashboard', '_pageDesc'=>'Welcome to admin panel as member'])

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-blue"><i class="fa fa-home"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Works</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-purple"><i class="fa fa-eye"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-comment"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Contact</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Subscribers</span>
                <span class="info-box-number">21</span>
              </div>
            </div>
        </div>
    </div>
@endsection