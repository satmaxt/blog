@extends('layouts.admin', ['_pageTitle'=>'Edit Portfolio', '_pageDesc'=>'Menambahkan portfolio baru'])

@section('title', 'Tambah Portfolio')

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.css') }}">
<style type="text/css">
    .widgets:first-child {
        padding-top: 0;
    }
    .widgets {
        padding: 20px 0;
        border-bottom: 1px solid #f0f0f0;
    }
    .widgets>h3 {
        padding: 0;
        margin: 0px 0px 15px 0px;
    }
</style>
@endsection

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Portfolio</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            @if (session()->has('flash_message.subject'))
            <div class="alert alert-{{ session()->get('flash_message.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>{{ session()->get('flash_message.subject') }}</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @elseif(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif

            {!! Form::open(['url'=>route('member.portfolio.update',['id'=>$portfolio->id]), 'id'=>'formmm', 'files'=>true, 'method'=>'patch']) !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-10">
                            {!! Form::text('judul', $portfolio->judul, ['class'=>'form-control','placeholder'=>'Judul portfolio']) !!}
                        </div>
                        <div class="col-sm-2"><button type="submit" class="btn btn-primary btn-block">Save</button></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">{!! Form::textarea('embed', $portfolio->embed, ['class'=>'form-control', 'id'=>'video', 'style'=>'display:none;', 'placeholder'=>'Video embed here.']) !!}</div>
                        <div class="form-group">
                            {!! Form::textarea('desc', $portfolio->desc, ['class'=>'form-control', 'id'=>'arti']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="widgets">
                            <h3>Thumbnail</h3>
                            <div class="form-group">
                              <input type="file" class="form-control" name="thumbnail">
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Template</h3>
                            <div class="form-group">
                                <select id="template" class="form-control">
                                    <option value>Pilih template (wajib)</option>
                                    <option value="1" {{ $portfolio->embed != "" ? "selected" : "" }}>Video</option>
                                    <option value="2" {{ $portfolio->embed == "" ? "selected" : "" }}>Artikel</option>
                                </select>
                            </div>
                            <div class="form-group" id="durasi" style="display: none;">
                                <label class="control-label">Durasi Video</label>
                                {!! Form::text('durasi', $portfolio->durasi, ['class'=>'form-control','placeholder'=>'Kosongkan jika tidak ada']) !!}
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Kategori</h3>
                            <div class="form-group">
                                <select class="selectpicker" name="label" data-live-search="true">
                                  @foreach ($labels as $label)
                                    <option value="{{ $portfolio->Labels()->first()->id }}" selected>{{ $portfolio->Labels()->first()->label }}</option>
                                    <option value="{{ $label->id }}">{{ $label->label }}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="widgets">
                            <h3>Pengerjaan</h3>
                            <div class="form-group">
                                <select id="kelompok" class="form-control">
                                    <option value>Select metode -</option>
                                    <option value="1" {{ $portfolio->kelompok != "" ? "selected" : "" }}>Berkelompok</option>
                                    <option value="2" {{ $portfolio->kelompok == "" ? "selected" : "" }}>Individu</option>
                                </select>
                            </div>
                            <div class="form-group" id="selkelompok" style="display: none;">
                                {!! Form::text('namakelompok', $portfolio->kelompok, ['class'=>'form-control', 'placeholder'=>'Nama kelompok']) !!}
                            </div>
                            <div class="form-group" id="anggota" style="display: none;">
                                {!! Form::text('anggota', $portfolio->anggota, ['class'=>'form-control', 'placeholder'=>'Anggota']) !!}
                                <span class="help-block">Gunakan ';' untuk pemisah anggota. Misalkan jhon;doe;samantha</span>
                            </div>
                            <div class="form-group" id="individu" style="display: none;">
                                {!! Form::text('namaindividu', $portfolio->author, ['class'=>'form-control', 'placeholder'=>'Nama creator']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('admin/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
      selector: 'textarea#arti',
      height: 400,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });

    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
          size: 10,
          width: '100%',
        });
    });

    $(document).ready(function() {
        method = $('#kelompok');
        if(method.val() == "1") {
            $('#selkelompok').slideDown();
            $('#anggota').slideDown();
            $('#individu').slideUp();
            $('input[name="namaindividu"]').val('');
        } else if (method.val() == "2") {
            $('#selkelompok').slideUp();
            $('#anggota').slideUp();
            $('input[name="namakelompok"]').val('');
            $('input[name="anggota"]').val('');
            $('#individu').slideDown();
        }
        method.on('change', function(e) {
            e.preventDefault();
            if(method.val() == "1") {
                $('#selkelompok').slideDown();
                $('#anggota').slideDown();
                $('#individu').slideUp();
                $('input[name="namaindividu"]').val('');
            } else if (method.val() == "2") {
                $('#selkelompok').slideUp();
                $('#anggota').slideUp();
                $('input[name="namakelompok"]').val('');
                $('input[name="anggota"]').val('');
                $('#individu').slideDown();
            }
        });
    });
    $(document).ready(function() {
        element = $('#template');
        if(element.val() == "1") {
            $('#video').slideDown();
            $('#durasi').slideDown();
        } else if (element.val() == "2") {
            $('#video').slideUp();
            $('#durasi').slideUp();
            $('input[name="durasi"]').val('');
            $('textarea[name="embed"]').val('');
        }
        element.on('change', function() {
            if(element.val() == "1") {
                $('#video').slideDown();
                $('#durasi').slideDown();
            } else if (element.val() == "2") {
                $('#video').slideUp();
                $('#durasi').slideUp();
                $('input[name="durasi"]').val('');
                $('textarea[name="embed"]').val('');
            }
        });
    });
</script>
@stop;
