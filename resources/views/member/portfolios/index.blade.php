@extends('layouts.admin', ['_pageTitle'=>'Portfolio', '_pageDesc'=>'Daftar portfolio yang telah di publish,'])

@section('title', 'Manage Portfolio')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Portfolio</h3>
            <a href='{!! route('member.portfolio.create') !!}' class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Baru</a>
        </div>
        <div class="box-body">
            @if (session()->has('flash_message.message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sukses!</strong> <br>
                {{ session()->get('flash_message.message') }}
            </div>
            @elseif(count($errors) > 0)
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif

            @if (count($portfolios) < 1)
            <span>Tidak ada data.</span>
            @else
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Judul</th>
                          <th>Mode</th>
                          <th>Kategori</th>
                          <th>Pada</th>
                          <th>Oleh</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="result-faker">
                        <?php $i = 1+($portfolios->perPage() * 0)+$portfolios->currentPage()-1; ?>
                        @foreach ($portfolios as $portfolio)
                          <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $portfolio->judul }}</td>
                            <td><span class="label label-primary">{{ $portfolio->kelompok == "" ? 'Individu' : 'Kelompok' }}</span></td>
                            <td>{{ $portfolio->Labels()->first()->label }}</td>
                            <td>{{ date("d M Y", strtotime($portfolio->created_at)) }}</td>
                            <td>{{ $portfolio->User->name }}</td>
                            <td>
                              <a href="{!! route('member.portfolio.edit', ['id'=>$portfolio->id]) !!}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                              {!! Form::open(['url'=>route('member.portfolio.destroy',['id'=>$portfolio->id]), 'method'=>'delete','class'=>'form-delete', 'style'=>'display:inline-block !important;']) !!}
                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                              {!! Form::close() !!}
                            </td>
                          </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
            {!! $portfolios->render() !!}
        </div>
    </div>
@endsection
@section('footer-scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.form-delete').on('submit', function() {
        konf = confirm('Apakah anda yakin  mau menghapus data ini?');
        if(konf) {
            return konf;
        } else {
            return false;
        }
    });
});
</script>
@stop
