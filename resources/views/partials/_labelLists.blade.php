<?php $i = 1+($labels->perPage() * 0)+$labels->currentPage()-1; ?>
@foreach ($labels as $label)
<tr>
    <td>{{ $i++ }}</td>
    <td>{{ $label->label }}</td>
    <td></td>
    <td>
        <a href="#modal-edit" data-toggle="modal" data-url="{{ route('admin.label.edit', ['id'=>$label->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
        <button data-delete="delete" data-url="{{ route('admin.label.destroy', ['id'=>$label->id]) }}" class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
    </td>
</tr>
@endforeach
<script src="{{ asset('admin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>
    $('button[data-delete="delete"]').on('click', function(e) {
            e.preventDefault;

            konf = confirm('Apakah anda yakin  mau menghapus data ini?');
            if(!konf) {
                return false;
            }
            
            data = $(this).attr('data-url');

            $.ajaxSetup({
                headers: {'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
            });

            $.ajax({
                url : data,
                type : 'delete',
                success: function(data) {
                    obj = sukses.find('.isi').append(data.message);
                    sukses.slideDown();
                    $('#faker').load('label/create');
                }
            });
        });
</script>