{!! Form::model($data,['url'=>route('admin.saids.update',['id'=>$data->id]), 'class'=>'form-horizontal', 'id'=>'form-edit', 'method'=>'patch']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Data</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <label class="control-label col-sm-2">Nama</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Sebagai</label>
        <div class="col-sm-10">
            {!! Form::text('passion', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Content</label>
        <div class="col-sm-10">
            {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>
{!! Form::close() !!} 