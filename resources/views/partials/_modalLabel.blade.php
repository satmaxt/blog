<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Label : {{ $label->label }}</h4>
</div>
<div class="modal-body">
    <form action="" method="POST" class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-2">Label</label>
			<div class="col-sm-10">
				<input type="hidden" name="_token" value="{{ csrf_token() }}" style="display: none;">
				<input type="text" name="label" id="fakerrrrr" class="form-control" value="{{ $label->label }}">
			</div>
		</div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id="modal-save">Save changes</button>
</div>
<script src="{{ asset('admin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>
	$(document).ready(function() {
		$('#modal-save').click(function(e) {
			e.preventDefault;
			info = $('#info');
			sukses = $('#success');

			$.ajaxSetup({
				headers : {'X-CSRF-Token' : $('input[name="_token"]').val() }
			});

			$.ajax({
				url : 'label/{{ $label->id }}',
				type : 'patch',
				data : {
					'label' : $('#fakerrrrr').val()
				},
				success : function(data) {
					if(!data.success) {
						obj = info.find('ul').empty();
                        $.each(data.message, function(index, message) {
                            obj.append('<li>'+message+'</li>');
                        });
                        info.slideDown();
					} else {
						sukses.find('.isi').text(' ');
                        obj = sukses.find('.isi').append(data.message);
                        sukses.slideDown();

                        $('#faker').load('label/create');
					}
				}
			});
		});
	});
</script>