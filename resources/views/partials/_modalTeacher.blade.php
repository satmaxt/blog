{!! Form::model($teacher, ['url'=>route('admin.teacher.update', ['id'=>$teacher->id]),'method'=>'patch', 'files'=>true]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Guru : {{ $teacher->nama }}</h4>
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>Nama</th>
                    <td>{!! Form::text('nama', null, ['class'=>'form-control']) !!}</td>
                </tr>
                <tr>
                    <th>Pelajaran</th>
                    <td>{!! Form::text('mapel', null, ['class'=>'form-control']) !!}</td>
                </tr>
                <tr>
                    <th>Foto</th>
                    <td>{!! Form::file('foto', ['class'=>'form-control']) !!}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" id="add-guru" class="btn btn-primary">Save</button>
</div>
{!! Form::close() !!}