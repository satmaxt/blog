{!! Form::model($user, ['url'=>route('admin.user.update', ['id'=>$user->id]), 'method'=>'patch']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit User : {{ $user->name }}</h4>
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <tbody>
                <tr>
                    <th>Name</th>
                    <td>{!! Form::text('name', null, ['class'=>'form-control']) !!}</td>
                </tr>
                <tr>
                    <th>Username</th>
                    <td>{!! Form::text('username', null, ['class'=>'form-control']) !!}</td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td>{!! Form::password('password', ['class'=>'form-control']) !!}</td>
                </tr>
                <tr>
                    <th>Role</th>
                    <td>{!! Form::select('role', ['1'=>'Admin', '2'=>'Member'], $user->role_id, ['class'=>'form-control']) !!}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
{!! Form::close() !!}