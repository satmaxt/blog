<?php $i = 1+($saids->perPage() * 0)+$saids->currentPage()-1; ?>
@foreach ($saids as $said)
<tr>
    <td>{{ $i++ }}</td>
    <td>{{ $said->name }}</td>
    <td>{{ $said->passion }}</td>
    <td>{{ str_limit($said->content, 50) }}</td>
    <td><a data-toggle="modal" href='#modal-edit' data-url="{{ route('admin.saids.edit',['id'=>$said->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
    {!! Form::open(['url'=>route('admin.saids.destroy', ['id'=>$said->id]), 'method'=>'delete', 'style'=>'display:inline-block !important','class'=>'form-delete']) !!}
    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
    {!! Form::close() !!}
    </td>
</tr>
@endforeach