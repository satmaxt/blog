<?php $i = 1+($services->perPage() * 0)+$services->currentPage()-1; ?>
@foreach ($services as $service)
<tr>
    <td>{{ $i++ }}</td>
    <td>{{ $service->title }}</td>
    <td>{{ $service->desc }}</td>
    <td><i class="fa fa-2x fa-{{ $service->icon }}"></i></td>
    <td>
        <a href="{{ route('admin.service.edit', ['id'=>$service->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
        {!! Form::open(['url'=>route('admin.service.destroy', ['id'=>$service->id]),'method'=>'delete', 'style'=>'display:inline-block !important']) !!}
        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
        {!! Form::close() !!}
    </td>
</tr>
@endforeach