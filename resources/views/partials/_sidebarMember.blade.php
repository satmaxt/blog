<ul class="sidebar-menu">
  <li class="header">HEADER</li>
  <!-- Optionally, you can add icons to the links -->
  <li class="active"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
  <li class="treeview">
    <a href="#"><i class="fa fa-archive"></i> <span>Portfolio</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa fa-circle-o"></i> Add Portfolio</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> List</a></li>
    </ul>
  </li>
  <li><a href="{{ route('admin.label.index') }}"><i class="fa fa-tags"></i> <span>Labels</span></a></li>
</ul>
